defmodule Repo do
  def create(entity, random_number) do
    cond do
      random_number > 0 -> {:ok, entity}
      random_number < 0 -> {:error, "failed to create"}
      true -> "ozielzinho"
    end
  end

  def get(_id) do
    {:ok, %Game{title: "Breath of the Wild", console: "Switch"}}
  end

  def delete(id, random_number) do
    cond do
      random_number > 0 -> {:ok, id}
      random_number < 0 -> {:error, "failed to delete"}
      true -> "ozielzinho"
    end
  end
end

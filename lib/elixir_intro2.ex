defmodule ElixirIntro2 do
  @moduledoc """
  Documentation for `ElixirIntro2`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ElixirIntro2.hello()
      :world

  """
  def hello do
    :world
  end
end

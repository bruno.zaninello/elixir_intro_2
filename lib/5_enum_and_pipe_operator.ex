defmodule ListOfNumbers do
  require Integer

  def multiply_by(numbers, multiplier) do
    Enum.map(numbers, fn number -> number * multiplier end)
  end

  def filter_even(numbers) do
    Enum.filter(numbers, fn number -> Integer.is_even(number) end)
  end

  def sum_all(numbers) do
    Enum.reduce(numbers, fn number, accumulator -> accumulator + number end)
  end

  def do_it_all(numbers, multiplier) do
    numbers
    |> multiply_by(multiplier)
    |> filter_even()
  end
end

defmodule Game do
  alias Repo

  defstruct title: nil, console: nil, id: nil

  def create(title, console, random_number) do
    result =
      %Game{title: title, console: console}
      |> Repo.create(random_number)

    case result do
      {:ok, created_game} ->
        created_game

      {:error, error} ->
        "An error occurred"

      _ ->
        "wat"
    end
  end

  def delete(id, random_number) do
    with {:ok, _game} <- Repo.get(id),
         {:ok, _} <- Repo.delete(id, random_number) do
      {:ok, "game deleted"}
    else
      error -> error
      _ -> "unknown error"
    end
  end
end

defmodule Person do
  # underneath, structs are simply maps
  defstruct name: "Ed", age: 87, has_keyboard_with_cool_lights?: false

  def new do
    %Person{}
  end

  # Pattern matching works on function signatures
  def new(name, age, has_keyboard_with_cool_lights?) do
    %Person{
      name: name,
      age: age,
      has_keyboard_with_cool_lights?: has_keyboard_with_cool_lights?
    }
  end

  def are_you_a_cool_kid_based_on_your_keyboard?(%Person{} = person) do
    if person.has_keyboard_with_cool_lights? do
      true
    else
      false
    end
  end

  defp are_you_boring?(%Person{} = person) do
    if person.has_keyboard_with_cool_lights? == false do
      true
    else
      false
    end
  end
end
